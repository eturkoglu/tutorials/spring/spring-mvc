<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<html>

<head>
  <title>Student Registration</title>
</head>

<body>
  
  <form:form action="process" modelAttribute="student" method="post">
  	First Name: <form:input path="firstName"/>
  	
  	<br><br>
  	
  	Last Name: <form:input path="lastName"/>
  	
  	<br><br>
  	
  	Country: 
  	<form:select path="country">
  	  <form:option value="Brazil" label="Brazil" />
  	  <form:option value="France" label="France" />
  	  <form:option value="England" label="England" />
  	  <form:option value="Japan" label="Japan" />
  	</form:select>
  	
  	<br><br>
  	
  	Team: 
  	<form:select path="team">
  	  <form:options items="${student.teamOptions}"/>
  	</form:select>
  	
  	<br><br>
  	
  	City:
  	<form:select path="city">
  	  <form:options items="${cityOptions}"/>
  	</form:select>
  	
  	<br><br>
  	
  	Favorite Language: 
  	Java <form:radiobutton path="favoriteLanguage" value="Java" />
  	C# <form:radiobutton path="favoriteLanguage" value="C#" />
  	PHP <form:radiobutton path="favoriteLanguage" value="PHP" />
  	Ruby <form:radiobutton path="favoriteLanguage" value="Ruby" />
  	
  	<br><br>
  	
  	Operating Systems: 
  	MS Windows<form:checkbox path="operatingSystems" value="MS Windows"/>
  	Mac OS<form:checkbox path="operatingSystems" value="Mac OS"/>
  	Linux<form:checkbox path="operatingSystems" value="Linux"/>
  	
  	<br><br>
  	
  	<input type="submit" value="Submit" />
  	
  </form:form>

</body>

</html>