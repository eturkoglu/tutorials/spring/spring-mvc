package com.emrah.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/hello")
public class HelloWorldController {

	@GetMapping("/show")
	public String show() {
		return "hello-form";
	}
	
	@GetMapping("/process")
	public String process() {
		return "hello-world";
	}
	
	@GetMapping("/process2")
	public String process2(HttpServletRequest request, Model model) {
		var studentName = request.getParameter("studentName");
		var message = "Yo! " + studentName.toUpperCase();
		model.addAttribute("message", message);
		return "hello-world";
	}
	
	@GetMapping("/process3")
	public String process3(@RequestParam("studentName") String studentName, Model model) {
		var message = "Hey! " + studentName.toUpperCase();
		model.addAttribute("message", message);
		return "hello-world";
	}
	
}
