package com.emrah.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.emrah.form.Student;

@Controller
@RequestMapping("/student")
public class StudentController {
	
	@Value("#{cityOptions}") 
	private Map<String, String> cityOptions;

	@GetMapping("/show")
	public String show(Model model) {
		model.addAttribute("student", new Student());
		model.addAttribute("cityOptions", cityOptions);
		return "student-form";
	}
	
	@PostMapping("/process")
	public String process(@ModelAttribute("student") Student student) {
		System.out.println("Student Name: " + student.getFirstName() + " " + student.getLastName());
		return "student-confirmation";
	}
	
}
