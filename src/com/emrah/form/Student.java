package com.emrah.form;

import java.util.LinkedHashMap;

public class Student {
	
	private String firstName;
	private String lastName;
	private String country;
	private String team;
	private String city;
	private String favoriteLanguage;
	private String[] operatingSystems;
	
	private LinkedHashMap<String, String> teamOptions;
	
	public Student() {
		teamOptions = new LinkedHashMap<String, String>();
		teamOptions.put("FB", "Fenerbahce");
		teamOptions.put("GS", "Galatasaray");
		teamOptions.put("BJK", "Besiktas");
		teamOptions.put("TS", "Trabzonspor");
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public LinkedHashMap<String, String> getTeamOptions() {
		return teamOptions;
	}

	public void setTeamOptions(LinkedHashMap<String, String> teamOptions) {
		this.teamOptions = teamOptions;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getFavoriteLanguage() {
		return favoriteLanguage;
	}

	public void setFavoriteLanguage(String favoriteLanguage) {
		this.favoriteLanguage = favoriteLanguage;
	}

	public String[] getOperatingSystems() {
		return operatingSystems;
	}

	public void setOperatingSystems(String[] operatingSystems) {
		this.operatingSystems = operatingSystems;
	}
	
}
