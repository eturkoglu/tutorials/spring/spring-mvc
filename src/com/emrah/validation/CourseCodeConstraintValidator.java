package com.emrah.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CourseCodeConstraintValidator implements ConstraintValidator<CourseCode, String>{

	//,,private String coursePrefix;
	private String[] coursePrefixes;
	
	@Override
	public void initialize(CourseCode courseCode) {
		//coursePrefix = courseCode.value();
		coursePrefixes = courseCode.value();
	}
	
	@Override
	public boolean isValid(String code, ConstraintValidatorContext constraintValidatorContext) {
		var result = true;
		
		//result = code != null ? code.startsWith(coursePrefix) : true;
		
		if (code != null) {
			for (String tempPrefix : coursePrefixes) {
				result = code.startsWith(tempPrefix);
                
                if (result) {
                    break;
                }
            }
        }
		
		return result;
	}

}

